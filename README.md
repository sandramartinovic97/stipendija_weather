**Five days weather forecast**

In order to use this application it is necessary to register on the following link:
'https://openweathermap.org/'. After registering, you will get your own API key.

In the 'application.properties' in 'src.main.resources' you can add your API key like this:
'app.weather.api.key=yourAPIkey'.

In this app you can use three custom REST endpoints:

**1.** Getting all default cities (cities specified in app)
(**http://localhost:8080/city/defaults -> GET**)

Result:

Default cities: 
Barcelona, Rome, Reykjavik

**2.** Getting average temperatures for those three specified cites for the next five days or getting average
temperatures for forwarded cities for the next five days
(**http://localhost:8080/city/averageForFiveDays -> POST**)

a) If no cities were passed (forward '[]' in body as a request), this REST endpoint returns list of three default
cities with their average temperature per day for the next five days.

As a result in response we get default cities:

[
    "City: Barcelona, temperatures: 29.05.2020 -> 21.92, 30.05.2020 -> 20.61, 31.05.2020 -> 21.53, 01.06.2020 -> 22.11, 02.06.2020 -> 21.71.",
    "City: Rome, temperatures: 29.05.2020 -> 17.23, 30.05.2020 -> 17.73, 31.05.2020 -> 18.88, 01.06.2020 -> 20.52, 02.06.2020 -> 21.26.",
    "City: Reykjavik, temperatures: 29.05.2020 -> 9.9, 30.05.2020 -> 8.86, 31.05.2020 -> 7.64, 01.06.2020 -> 8.42, 02.06.2020 -> 8.76."
]

b) If we pass some random cities, we get their average temperature per day for the next five days.

If we pass:

[{"name":"London","country":"GB"},{"name":"Berlin","country":"DE"},{"name":"Los Angeles","country":"US"}]

As a result in response we get random cities:

[
    "City: London, temperatures: 29.05.2020 -> 16.21, 30.05.2020 -> 16.54, 31.05.2020 -> 17.43, 01.06.2020 -> 17.27, 02.06.2020 -> 18.08.",
    "City: Berlin, temperatures: 29.05.2020 -> 14.12, 30.05.2020 -> 13.47, 31.05.2020 -> 15.11, 01.06.2020 -> 18.66, 02.06.2020 -> 19.58.",
    "City: Los Angeles, temperatures: 29.05.2020 -> 22.12, 30.05.2020 -> 20.4, 31.05.2020 -> 21.0, 01.06.2020 -> 22.3, 02.06.2020 -> 21.4."
]

**3.** Getting default cities sorted by average temperatures
(**http://localhost:8080/city/sortAveragePerCity -> POST**)

If no cities were passed, this REST endpoint returns list of three default cities sorted by their average temperature from
the current day.

If we pass random cities:

[{"name":"London","country":"GB"},{"name":"Berlin","country":"DE"},{"name":"Los Angeles","country":"US"}]

As a result in response we get:

{
   "Berlin": 14.25,
   "London": 16.30,
   "Los Angeles": 22.24
}