package com.stipendija.stipendija_weather.controller;

import com.stipendija.stipendija_weather.model.Weather;
import com.stipendija.stipendija_weather.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Weather controller contains one REST endpoints for calling OpenWeatherMap API.
 */
@RestController
@RequestMapping("/weather")
public class WeatherController {

    @Autowired
    private WeatherService weatherService;

    /**
     * Returns current temperature for the forwarded city.
     */
    @GetMapping("/now/{country}/{city}")
    public Weather getWeather(@PathVariable String country, @PathVariable String city) {
        return weatherService.getWeather(country, city);
    }
}