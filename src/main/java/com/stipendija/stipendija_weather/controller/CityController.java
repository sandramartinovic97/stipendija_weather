package com.stipendija.stipendija_weather.controller;

import com.stipendija.stipendija_weather.model.City;
import com.stipendija.stipendija_weather.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * City controller contains three REST endpoints for calling OpenWeatherMap API.
 */
@RestController
@RequestMapping("/city")
public class CityController {

    @Autowired
    private CityService cityService;

    /**
     * Returns default cities (cities from configuration).
     */
    @GetMapping("/defaults")
    public String getDefaultCities() { return cityService.getDefaultCities(); }

    /**
     * Returns average temperatures for five days for default or forwarded cities.
     */
    @PostMapping(value = "/averageForFiveDays", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getAverageTemp(@RequestBody List<City> cities) {
        return cityService.getAverageTemp(cities);
    }

    /**
     * Returns sorted cities by average temperature.
     */
    @PostMapping(value = "/sortAveragePerCity", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Double> sortPerCity(@RequestBody List<City> cities) { return cityService.sortPerCityTemperature(cities); }
}