package com.stipendija.stipendija_weather.controller;

import com.stipendija.stipendija_weather.service.ForecastService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Forecast controller contains one REST endpoints for calling OpenWeatherMap API.
 */
@RestController
@RequestMapping("/forecast")
public class ForecastController {

    @Autowired
    private ForecastService forecastService;

    /**
     * Returns average temperatures for five days for forwarded city.
     */
    @GetMapping("/days/{country}/{city}")
    public Map<String, Double> getForecast(@PathVariable String country, @PathVariable String city) {
        return forecastService.getAverage(country, city);
    }
}