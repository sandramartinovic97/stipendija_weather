package com.stipendija.stipendija_weather.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This entity class presents model for City with his attributes.
 */
@Data
@Entity
public class City {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String country;

    @JsonIgnoreProperties("country")
    @OneToMany
    private List<Weather> cities = new ArrayList<>();

    @JsonProperty("cities")
    public List<Weather> getCities() { return this.cities; }

    @JsonSetter("list")
    public void setCities(List<Weather> cities) { this.cities = cities; }
}