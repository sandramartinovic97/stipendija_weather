package com.stipendija.stipendija_weather.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This entity class presents model for Forecast with his attributes.
 */
@Data
@Entity
public class Forecast {

    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String city;

    @OneToMany
    private List<Weather> days = new ArrayList<>();

    @JsonIgnoreProperties({"id", "name", "country"})
    @JsonProperty("days")
    public List<Weather> getDays() { return this.days; }

    @JsonSetter("list")
    public void setDays(List<Weather> days) { this.days = days; }

    @JsonProperty("city")
    public void setCityName(Map<String, Object> city) { setCity(city.get("name").toString()); }
}