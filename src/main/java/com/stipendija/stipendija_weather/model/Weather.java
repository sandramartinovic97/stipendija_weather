package com.stipendija.stipendija_weather.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Data;

import javax.persistence.*;
import java.time.Instant;
import java.util.Map;

/**
 * This entity class presents model for Weather with his attributes.
 */
@Data
@Entity
public class Weather {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String city;

    @JsonIgnore
    private String country;

    private double temperature;

    private Instant dateTime;

    public Weather() {}

    @JsonProperty("name")
    public String getCity() { return this.city; }

    @JsonProperty("date_time")
    public Instant getDateTime() {
        return this.dateTime;
    }

    @JsonSetter("dt")
    public void setDateTime(long unixTime) { this.dateTime = Instant.ofEpochMilli(unixTime * 1000); }

    @JsonProperty("main")
    public void setMain(Map<String, Object> main) { setTemperature(Double.parseDouble(main.get("temp").toString())); }
}