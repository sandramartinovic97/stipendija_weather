package com.stipendija.stipendija_weather.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * This configuration entity class uses @ConfigurationProperties
 * annotation in order to read from 'application.properties' file.
 */
@ConfigurationProperties("app.weather")
public class WeatherConfig {

    @Valid
    private final Api api = new Api();

    /**
     *  With @Value annotation we want to inject value 'app.weather.location'
     *  from 'application.properties' file.
     */
    @Value(value = "${app.weather.location}")
    private String[] listOfLocation;

    public Api getApi() { return api; }

    public String[] getListOfLocation() { return listOfLocation; }

    /**
     *  API key from OpenWeatherMap service.
     */
    public static class Api {

        @NotNull
        private String key;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }
}