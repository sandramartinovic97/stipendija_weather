package com.stipendija.stipendija_weather;

import com.stipendija.stipendija_weather.config.WeatherConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;

/**
 *  Annotation @EnableConfigurationProperties is added for enabling
 *  support of @ConfigurationProperties in WeatherConfig class.
 */
@SpringBootApplication
@EnableConfigurationProperties(WeatherConfig.class)
@EnableCaching(proxyTargetClass = true)
public class StipendijaWeatherApplication {

	public static void main(String[] args) {
		SpringApplication.run(StipendijaWeatherApplication.class, args);
	}

}