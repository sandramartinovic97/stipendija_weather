package com.stipendija.stipendija_weather.service;

import com.stipendija.stipendija_weather.config.WeatherConfig;
import com.stipendija.stipendija_weather.model.Weather;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import java.net.URI;

/**
 *  This class contains implementation of one public and one private method.
 *
 */
@Service
public class WeatherServiceImpl implements WeatherService {

    /**
     * In order to access OpenWeatherMap site, it is necessary to declare
     * this static variable.
     */
    private static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q={city},{country}&APPID={key}&units=metric";
    private final RestTemplate restTemplate;
    private final String apiKey;

    public WeatherServiceImpl(RestTemplateBuilder restTemplateBuilder, WeatherConfig weatherConfig) {
        this.restTemplate = restTemplateBuilder.build();
        this.apiKey = weatherConfig.getApi().getKey();
    }

    @Override
    public Weather getWeather(String country, String city) {
        URI url = new UriTemplate(WEATHER_URL).expand(city, country, apiKey);
        return invoke(url, Weather.class);
    }

    private <T> T invoke(URI url, Class<T> responseType) {
        RequestEntity<?> request = RequestEntity.get(url)
                .accept(MediaType.APPLICATION_JSON).build();
        ResponseEntity<T> exchange = restTemplate.exchange(request, responseType);
        return exchange.getBody();
    }
}