package com.stipendija.stipendija_weather.service;

import com.stipendija.stipendija_weather.config.WeatherConfig;
import com.stipendija.stipendija_weather.model.Forecast;
import com.stipendija.stipendija_weather.model.Weather;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 *  This class contains implementation of two public and one private method.
 *
 */
@Service
public class ForecastServiceImpl implements ForecastService {

    /**
     * In order to access OpenWeatherMap site, it is necessary to declare
     * this static variable.
     */
    private static final String FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast?q={city},{country}&APPID={key}&units=metric";
    private final RestTemplate restTemplate;
    private final String apiKey;
    private static final int numOfHours = 8;

    public ForecastServiceImpl(RestTemplateBuilder restTemplateBuilder, WeatherConfig weatherConfig) {
        this.restTemplate = restTemplateBuilder.build();
        this.apiKey = weatherConfig.getApi().getKey();
    }

    @Override
    public Forecast getForecast(String country, String city) {
        URI url = new UriTemplate(FORECAST_URL).expand(city, country, apiKey);
        return invoke(url, Forecast.class);
    }

    private <T> T invoke(URI url, Class<T> responseType) {
        RequestEntity<?> request = RequestEntity.get(url)
                .accept(MediaType.APPLICATION_JSON).build();
        ResponseEntity<T> exchange = restTemplate.exchange(request, responseType);
        return exchange.getBody();
    }

    /**
     * With this method we can calculate average temperature per day
     * for one city for the next five days.
     */
    @Override
    public Map<String, Double> getAverage(String country, String city) {
        // OpenWeatherMap API returns forecast for the next 5 days per 8 hours in a day
        Forecast forecastForCity = getForecast(country, city);
        // get all 40 days
        List<Weather> listOfDays = forecastForCity.getDays();

        // nicely formatted dates
        List<String> finalDays = formatDates(listOfDays);

        // take all temperatures for all hours
        List<Double> listOfTemperatures = listOfDays.stream()
                .map(Weather::getTemperature)
                .collect(Collectors.toList());

        final AtomicInteger counter = new AtomicInteger();
        // group temperatures per one day, since there are 8 hours in one day, result will be collection of 5 groups
        Collection<List<Double>> temperaturesPerDays = listOfTemperatures.stream()
                .collect(Collectors.groupingBy(i -> counter.getAndIncrement() / numOfHours)).values();
        // get average temp per one day in every of 5 groups
        List<Double> averagePerDay = temperaturesPerDays.stream().map(w -> w.stream()
                .mapToDouble(i -> i)
                .average().getAsDouble()).collect(Collectors.toList());
        // create a map that contains temperature per date
        Map<String, Double> map = new LinkedHashMap<>();
        for (int i = 0; i < finalDays.size(); i++) {
            BigDecimal average = new BigDecimal(averagePerDay.get(i)).setScale(2, RoundingMode.HALF_UP);
            map.put(finalDays.get(i), average.doubleValue());
        }
        return map;
    }

    /**
     * Date formatting is implemented in this method.
     */
    private List<String> formatDates(List<Weather> listOfDays){
        // structure readable date format e.g. 24.08.2020
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        List<Instant> instantDates = listOfDays.stream()
                .map(Weather::getDateTime)
                .collect(Collectors.toList());
        List<Date> dates = instantDates.stream().map(d -> Date.from(d)).collect(Collectors.toList());
        List<String> formatDates = dates.stream().map(dd -> formatter.format(dd)).collect(Collectors.toList());

        return formatDates.stream().distinct().limit(5).collect(Collectors.toList());
    }
}