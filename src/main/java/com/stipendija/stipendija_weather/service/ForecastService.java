package com.stipendija.stipendija_weather.service;

import com.stipendija.stipendija_weather.model.Forecast;

import java.util.Map;

/**
 * This service contains declaration of two methods.
 */
public interface ForecastService {
    Forecast getForecast(String country, String city);
    Map<String, Double> getAverage(String country, String city);
}