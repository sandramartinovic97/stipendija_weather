package com.stipendija.stipendija_weather.service;

import com.stipendija.stipendija_weather.config.WeatherConfig;
import com.stipendija.stipendija_weather.controller.ForecastController;
import com.stipendija.stipendija_weather.model.City;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 *  This class contains implementation of three methods.
 *
 */
@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private WeatherConfig weatherConfig;

    @Autowired
    private ForecastController forecastController;

    private List<City> defaultCities;

    /**
     * Returns default cities which are stored in configuration.
     */
    @Override
    public String getDefaultCities() {
        List<String> cities = new ArrayList<>();
        defaultCities = new ArrayList<>();

        for (String location : weatherConfig.getListOfLocation()) {
            String country = location.split("/")[0];
            String cityName = location.split("/")[1];
            cities.add(cityName);
            City city = new City();
            city.setName(cityName);
            city.setCountry(country);
            defaultCities.add(city);
        }

        return "Default cities: \n" + cities.toString().replace("[", "").replace("]", "");
    }

    /**
     * If no cities were passed, this method returns list of three
     * default cities with their average temperature per day for
     * the next five days.
     * If we pass some random cities, we get their average temperature
     * per day for the next five days.
     */
    @Override
    public List<String> getAverageTemp(List<City> cities) {
        List<String> result = new ArrayList<>();
        // if no cities were passed use default cities to get average temperatures
        if (cities == null || cities.isEmpty()) {
            getDefaultCities();
            cities = defaultCities;
        }
        for (City city : cities) {
            Map<String, Double> averagePerCity = forecastController.getForecast(city.getCountry(), city.getName());
            result.add("City: " + city.getName() + ", temperatures: " + averagePerCity.toString().replace("=", " -> ")
                    .replace("{", "").replace("}", "") + ".");
        }
        return result;
    }

    /**
     * If no cities were passed, this method returns list of three
     * default cities sorted by their average temperature from
     * the current day.
     * If we pass some random cities, we sort them by their average
     * temperature from the current day.
     */
    public Map<String, Double> sortPerCityTemperature(List<City> cities) {
        Map<String, Double> result = new LinkedHashMap<>();
        // if no cities were passed use default cities to get average temperatures
        if (cities == null || cities.isEmpty()) {
            getDefaultCities();
            cities = defaultCities;
        }
        for (City city : cities) {
            Map<String, Double> averagePerCity = forecastController.getForecast(city.getCountry(), city.getName());
            // takes first element of the map
            Map.Entry<String,Double> entry = averagePerCity.entrySet().iterator().next();
            // takes key of the first element, i.e. today's date
            String key= entry.getKey();
            result.put(city.getName(), averagePerCity.get(key));
        }

        Map<String, Double> sortedByTemperature = result.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        return sortedByTemperature;
    }
}