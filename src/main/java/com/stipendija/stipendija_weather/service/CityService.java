package com.stipendija.stipendija_weather.service;

import com.stipendija.stipendija_weather.model.City;

import java.util.List;
import java.util.Map;

/**
 * This service contains declaration of three methods.
 */
public interface CityService {
    String getDefaultCities();
    List<String> getAverageTemp(List<City> cities);
    Map<String,Double> sortPerCityTemperature(List<City> cities);
}