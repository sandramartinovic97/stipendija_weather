package com.stipendija.stipendija_weather.service;

import com.stipendija.stipendija_weather.model.Weather;

/**
 * This service contains declaration of one method.
 */
public interface WeatherService {
    Weather getWeather(String country, String city);
}