package com.stipendija.stipendija_weather.controller;

import com.stipendija.stipendija_weather.model.Weather;
import com.stipendija.stipendija_weather.service.WeatherServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import static org.assertj.core.api.Assertions.assertThat;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(WeatherController.class)
public class WeatherControllerTest {

    @InjectMocks
    private WeatherController weatherController;

    @Mock
    private WeatherServiceImpl weatherServiceImpl;

    @Autowired
    private MockMvc mvc;

    @Test
    public void getWeather_noCountryOrCity_illegalStateExceptionThrown() {
        String country = "ES";
        String city = "Barcelona";

        when(weatherServiceImpl.getWeather(country, city)).thenThrow(new IllegalStateException());

        Assertions.assertThatThrownBy(() -> weatherController.getWeather(country, city)).isInstanceOf(IllegalStateException.class);
    }

    @Test
    public void getWeather_cityOrCountryExists_currentTemperatureForOneCityReturned() {
        String country = "ES";
        String city = "Barcelona";
        Weather weather = new Weather();

        when(weatherServiceImpl.getWeather(country, city)).thenReturn(weather);

        Weather result = weatherController.getWeather(country, city);

        verify(weatherServiceImpl).getWeather(country, city);
        assertThat(result).isEqualTo(result);
    }
}