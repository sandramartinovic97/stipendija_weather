package com.stipendija.stipendija_weather.controller;

import com.stipendija.stipendija_weather.model.City;
import com.stipendija.stipendija_weather.service.CityServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CityControllerTest {

    @InjectMocks
    private CityController cityController;

    @Mock
    private CityServiceImpl cityServiceImpl;

    @Test
    public void getDefaultCities_noCitiesInConfiguration_emptyStringReturned() {
        when(cityServiceImpl.getDefaultCities()).thenThrow(new IllegalStateException());

        Assertions.assertThatThrownBy(() -> cityController.getDefaultCities()).isInstanceOf(IllegalStateException.class);
    }

    @Test
    public void getDefaultCities_citiesInConfiguration_stringWithDefaultCitiesReturned() {
        String string = "Rome, Barcelona, Reykjavik";

        when(cityServiceImpl.getDefaultCities()).thenReturn(string);

        String result = cityController.getDefaultCities();

        verify(cityServiceImpl).getDefaultCities();
        assertThat(result);
        assertThat(result).contains(string);
    }


    @Test
    public void getAverageTemp_noDefaultCitiesOrForwardedCities_listOfDefaultCitiesOrForwardedCitiesReturned() {
        ArrayList<City> list = new ArrayList<City>();
        when(cityServiceImpl.getAverageTemp(list)).thenThrow(new IllegalStateException());

        Assertions.assertThatThrownBy(() -> cityController.getAverageTemp(list)).isInstanceOf(IllegalStateException.class);
    }

    @Test
    public void getAverageTemp_defaultCitiesOrForwardedCities_listOfDefaultCitiesOrForwardedCitiesReturned() {
        City city = new City();
        ArrayList<City> list = new ArrayList<City>();
        city.setName("Barcelona");
        city.setCountry("ES");
        list.add(city);

        List<String> strings = new ArrayList<>();
        strings.equals("City: London, temperatures: 29.05.2020 -> 16.09, 30.05.2020 -> 16.84, 31.05.2020 -> 17.48, 01.06.2020 -> 17.47, 02.06.2020 -> 18.23.");

        when(cityServiceImpl.getAverageTemp(list)).thenReturn(strings);

        List<String> result = cityController.getAverageTemp(list);

        assertThat(result);
    }
}